package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/Vallyenfail/geoCourier/module/courierfacade/service"
	"log"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(time.Millisecond * 50)
	courierStatus := c.courierService.GetStatus(ctx)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	ctx.JSON(200, courierStatus)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	mData, ok := m.Data.([]byte)
	if !ok {
		log.Fatal(err)
	}
	err = json.Unmarshal(mData, &cm)
	if err != nil {
		log.Fatal(err)
	}

	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
