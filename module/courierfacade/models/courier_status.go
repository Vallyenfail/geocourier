package models

import (
	cm "gitlab.com/Vallyenfail/geoCourier/module/courier/models"
	om "gitlab.com/Vallyenfail/geoCourier/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
