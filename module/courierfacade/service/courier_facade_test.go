package service

import (
	"context"
	cfm "gitlab.com/Vallyenfail/geoCourier/module/courierfacade/models"
	"gitlab.com/Vallyenfail/geoCourier/module/courierfacade/service/mocks"
	"testing"
)

func TestCourierFacade_GetStatus(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "First mock",
			args: args{ctx: context.Background()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			courierFacer := mocks.NewCourierFacer(t)

			courierFacer.On("GetStatus", tt.args.ctx).Return(cfm.CourierStatus{})

			c := courierFacer
			_ = c.GetStatus(tt.args.ctx)
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Second mock",
			args: args{
				ctx:       context.Background(),
				direction: 1,
				zoom:      12,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			courierFacer := mocks.NewCourierFacer(t)

			courierFacer.On("MoveCourier", tt.args.ctx, tt.args.direction, tt.args.zoom)

			c := courierFacer

			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
