package models

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Courier struct {
	Score    int   `json:"score"`
	Location Point `json:"location"`
}
