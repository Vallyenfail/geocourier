package service

import (
	"context"
	"gitlab.com/Vallyenfail/geoCourier/module/courier/models"
	"gitlab.com/Vallyenfail/geoCourier/module/courier/service/mocks"
	"testing"
)

func TestCourierService_GetCourier(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "First mock",
			args: args{ctx: context.Background()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			courierer := mocks.NewCourierer(t)

			courierer.On("GetCourier", tt.args.ctx).Return(&models.Courier{}, nil)

			c := courierer
			_, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Second mock",
			args: args{
				courier: models.Courier{
					Score: 0,
					Location: models.Point{
						Lat: 1,
						Lng: 2,
					},
				},
				direction: 1,
				zoom:      12,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			courierer := mocks.NewCourierer(t)

			courierer.On("MoveCourier", tt.args.courier, tt.args.direction, tt.args.zoom).Return(nil)

			c := courierer
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
