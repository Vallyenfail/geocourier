package service

import (
	"context"
	"gitlab.com/Vallyenfail/geoCourier/geo"
	"gitlab.com/Vallyenfail/geoCourier/module/courier/models"
	"gitlab.com/Vallyenfail/geoCourier/module/courier/storage"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

//go:generate mockery --name=Courierer
type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier_storage.go
	courier, err := c.courierStorage.GetOne(ctx)
	if err != nil {
		return nil, err
	}
	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	// сохраняем новые координаты курьера
	if courier == nil {
		courier = &models.Courier{
			Location: models.Point{
				Lat: DefaultCourierLat,
				Lng: DefaultCourierLng,
			},
		}
	}
	p := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	isTrue := geo.CheckPointIsAllowed(p, c.allowedZone, c.disabledZones)

	if !isTrue {
		np := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat = np.Lat
		courier.Location.Lng = np.Lng
	}

	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		return nil, err
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	distance := 0.001 / float64(2^(zoom-14))

	switch direction {
	case DirectionUp:
		courier.Location.Lat -= distance
	case DirectionDown:
		courier.Location.Lat += distance
	case DirectionLeft:
		courier.Location.Lng += distance
	case DirectionRight:
		courier.Location.Lng -= distance
	}
	// далее нужно проверить, что курьер не вышел за границы зоны
	p := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}
	isTrue := geo.CheckPointIsAllowed(p, c.allowedZone, c.disabledZones)

	// если вышел, то нужно переместить его в случайную точку внутри зоны
	if !isTrue {
		np := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat = np.Lat
		courier.Location.Lng = np.Lng
	}
	// далее сохранить изменения в хранилище
	err := c.courierStorage.Save(context.Background(), courier)

	return err
}
