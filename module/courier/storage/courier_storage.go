package storage

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"gitlab.com/Vallyenfail/geoCourier/module/courier/models"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	courierJSON, err := json.Marshal(courier)
	if err != nil {
		return err
	}
	err = c.storage.Set(ctx, "courier", courierJSON, 0).Err()
	if err != nil {
		panic(err)
	}

	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var courier models.Courier
	var data []byte
	var err error

	data, err = c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}
