package docs

import "gitlab.com/Vallyenfail/geoCourier/module/courierfacade/models"

// добавить документацию для роута /api/status courier defaultRequest

//swagger:route GET /api/status courier defaultRequest
//Getting courier
//responses:
//   200: statusResponse

//swagger:response statusResponse
type statusResponse struct {
	//in: body
	Courier models.CourierStatus `json:"courier"`
}
