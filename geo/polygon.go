package geo

import (
	"errors"
	"fmt"
	"github.com/brianvoe/gofakeit"
	geo "github.com/kellydunn/golang-geo"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func (p *Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}
func min(a, b float64) float64 {
	if a > b {
		return b
	}
	return a
}
func max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}
func (p *Polygon) RandomPoint() Point {
	//Берем 2 рандомные точки и находим точку между.

	points := p.polygon.Points()
	i := gofakeit.Number(0, len(points)-1)
	j := gofakeit.Number(0, len(points)-1)
	lng := gofakeit.Float64Range(min(points[i].Lng(), points[j].Lng()), max(points[i].Lng(), points[j].Lng()))
	lat := gofakeit.Float64Range(min(points[i].Lat(), points[j].Lat()), max(points[i].Lat(), points[j].Lat()))

	//log.Println("LOG OF POINTS IS", len(points), "LNG, LAT", lng, lat)
	return Point{
		Lat: lat,
		Lng: lng,
	}
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	geoPoints := make([]*geo.Point, len(points))
	for i, point := range points {
		geoPoints[i] = geo.NewPoint(point.Lat, point.Lng)
	}

	return &Polygon{
		polygon: geo.NewPolygon(geoPoints),
		allowed: allowed,
	}
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	// проверить, находится ли точка в разрешенной зоне
	for _, elem := range disabledZones {
		if elem.Contains(point) {
			return false
		}
	}
	if !allowedZone.Contains(point) {
		return false
	}

	return true
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var point Point
	//log.Println("Calling grt random allowed loco func")
	point = allowedZone.RandomPoint()
	for {
		point = allowedZone.RandomPoint()
		if CheckPointIsAllowed(point, allowedZone, disabledZones) {
			break
			//log.Println("Got allowed one")
		}
	}
	return point
}

func NewDisAllowedZone1() *Polygon {
	// добавить полигон с неразрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	expression := `(?s)var noOrdersPolygon1\s*=\s*\[(.*?)\];`
	points, err := ParsePoints(expression)
	if err != nil {
		fmt.Println(err)
		return &Polygon{}
	}
	return NewPolygon(points, false)
}

func NewDisAllowedZone2() *Polygon {
	// добавить полигон с неразрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	expression := `(?s)var noOrdersPolygon2\s*=\s*\[(.*?)\];`
	points, err := ParsePoints(expression)
	if err != nil {
		fmt.Println(err)
		return &Polygon{}
	}
	return NewPolygon(points, false)
}

func NewAllowedZone() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	expression := `(?s)var mainPolygon\s*=\s*\[(.*?)\];`
	points, err := ParsePoints(expression)
	if err != nil {
		fmt.Println(err)
		return &Polygon{}
	}
	return NewPolygon(points, true)
}

func ParsePoints(exp string) ([]Point, error) {
	filePath := "./public/js/polygon.js"

	content, err := os.ReadFile(filePath)
	if err != nil {
		fmt.Printf("Failed to read JavaScript file: %s\n", err.Error())
		return nil, errors.New("failed to read JavaScript file")
	}

	// Define a regular expression pattern to match the desired variable assignment
	pattern := exp

	re := regexp.MustCompile(pattern)
	match := re.FindStringSubmatch(string(content))

	if len(match) != 2 {
		return nil, errors.New("failed to find the variable")
	}

	jsArray := match[1]

	// Split the string by commas to extract individual points
	pointStrings := strings.Split(jsArray, ",\n")

	var points []Point

	// Iterate over each point string and parse latitude and longitude
	for _, pointStr := range pointStrings {
		if pointStr == "" {
			break
		}
		trimmed := strings.TrimSpace(pointStr)
		coordinates := strings.Split(trimmed, ",")
		var point []float64
		for _, coordinate := range coordinates {
			value := strings.Trim(coordinate, "[] ")
			// Parse the coordinate value as a float64 and append to the point slice
			coordinateValue, err := strconv.ParseFloat(value, 64)
			if err != nil {
				fmt.Printf("Failed to parse coordinate value: %s\n", err.Error())
				return nil, err
			}
			point = append(point, coordinateValue)
		}
		points = append(points, Point{
			Lat: point[0],
			Lng: point[1],
		})
	}
	return points, nil
}
